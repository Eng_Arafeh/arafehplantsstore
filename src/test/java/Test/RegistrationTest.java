package Test;

import Utilities.BrowsersBase;
import Utilities.Navigator;
import WebElementsUtilities.AccountPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegistrationTest extends BrowsersBase {

    AccountPage accountPage = new AccountPage(getDriver());
    Navigator navigator = null;
    private final String userName = accountPage.getUserName();
    private final String email = accountPage.getUserEmail();
    private final String password = accountPage.getUserPassword();
    private final String expectedResult = "Faisal_";

    @Test(priority = 1, alwaysRun = true)
    public void validRegistration() {
        accountPage = new AccountPage(getDriver());
        accountPage.registerANewUser(userName, email, password);

        Assert.assertTrue(accountPage.getActualResult().contains(expectedResult));

    }

    @Test(priority = 2, alwaysRun = true)
    public void emptyRegistration() {
        accountPage = new AccountPage(getDriver());
        accountPage.registerANewUser("", "", "");
        String actualResult = accountPage.getErrorMessage();

        Assert.assertEquals(actualResult, "Error: Please provide a valid email address.");
    }

    @Test(priority = 3, alwaysRun = true)
    public void invalidRegistration() {
        accountPage = new AccountPage(getDriver());
        accountPage.registerANewUser(userName, "Arafeh_4568@gmail", password);
        String actualResult = accountPage.getErrorMessage();

        Assert.assertEquals(actualResult, "Error: Please provide a valid email address.");
    }

    @Test(enabled = false, priority = 4)
    public void weakPasswordRegister() {
        accountPage = new AccountPage(getDriver());
        String weakPassword = "03004154958";
        accountPage.registerANewUser(userName, email, weakPassword);
        accountPage.getPasswordHint();
        String expectedResult = "Hint: The password should be at least twelve characters long. To make it stronger, use "
                +"upper and lower case letters, numbers, and symbols like ! \" ? $ % ^ & ).";
        String actualResult = accountPage.getPasswordHint();

        Assert.assertEquals(actualResult, expectedResult);}

    @Test(priority = 5, alwaysRun = true, enabled = false)
    public void goBackAfterRegistration() {
        accountPage = new AccountPage(getDriver());
        navigator = new Navigator(getDriver());
        accountPage.registerANewUser(userName, email, password);
        navigator.browserBackButton();
        navigator.goToMyAccountPage();
        Assert.assertTrue(accountPage.getActualResult().contains(expectedResult));
    }

    @Test(priority = 6, alwaysRun = true)
    public void alreadyRegisteredEmail() {
        accountPage = new AccountPage(getDriver());
        accountPage.registerANewUser(userName, "arafehhh_P@gmail.com", password);
        String actualResult = accountPage.getErrorMessage();
        String expectedErrorMsg = "Error: An account is already registered with your email address. Please log in.";

        Assert.assertEquals(actualResult, expectedErrorMsg);
    }

    @Test(priority = 7, alwaysRun = true)
    public void alreadyRegisteredUsername() {
        accountPage = new AccountPage(getDriver());
        accountPage.registerANewUser("arafehhh_P", email, password);

        Assert.assertTrue(accountPage.getErrorMessage().contains("already registered"));
    }

}