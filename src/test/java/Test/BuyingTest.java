package Test;

import Utilities.BrowsersBase;
import WebElementsUtilities.AccountPage;
import WebElementsUtilities.Cart;
import WebElementsUtilities.Checkout;
import WebElementsUtilities.Product;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BuyingTest extends BrowsersBase {

    Product product;
    Checkout checkout;
    AccountPage accountPage = new AccountPage(getDriver());
    LoginTest loginTest = new LoginTest();
    private String email = loginTest.getEmail();
    private String password = accountPage.getUserPassword();

    @Test(priority = 18, alwaysRun = true)
    public void buyProduct() {
        product = new Product(getDriver());
        checkout = new Checkout(getDriver());
        product.product(email, password, 7, "Jordan");
        String expectedResult = "Thank you. Your order has been received.";
        String actualResult = checkout.getSuccessCheckOutMessage();

        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test(priority = 19, alwaysRun = true)
    public void deleteProduct() {
        product = new Product(getDriver());
        Cart cart = new Cart(getDriver());
        product.product(email, password);

        Assert.assertTrue(cart.getCartMessage().contains("removed")); }
}
