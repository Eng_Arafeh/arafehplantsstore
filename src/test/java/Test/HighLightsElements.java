package Test;

import Utilities.BrowsersBase;
import WebElementsUtilities.AccountPage;
import Utilities.Navigator;
import WebElementsUtilities.Cart;
import WebElementsUtilities.Product;
import WebElementsUtilities.Store;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;


// This class was made to make sure the web elements are correctly located
public class HighLightsElements extends BrowsersBase {


    @Test(priority = 1)
    public void webElementsHighLights() throws InterruptedException {
        Navigator navigator = new Navigator(getDriver());

        highLight(navigator.getLogoIcon());
        highLight(navigator.getHomePage());
        highLight(navigator.getStorePage());
        highLight(navigator.getMenPage());
        highLight(navigator.getWomenPagePage());
        highLight(navigator.getAccessoriesPage());
        highLight(navigator.getMyAccountPage());
        highLight(navigator.getAboutPage());
        highLight(navigator.getContactUsPage());
        highLight(navigator.getCartPage());

        Thread.sleep(3000); // just to make sure elements located correctly
    }

    @Test(priority = 2)
    public void myAccountWebElementsHighLights() throws InterruptedException {

        Navigator navigator = new Navigator(getDriver());
        AccountPage accountPage = new AccountPage(getDriver());

        navigator.goToMyAccountPage();

       highLight(accountPage.getLoginEmailAddressTextBox());
        highLight(accountPage.getLoginPasswordTextBox());
        highLight(accountPage.getRememberMeCheckBox());
        accountPage.getRememberMeCheckBox().click();
        highLight(accountPage.getLoginButton());
        highLight(accountPage.getLoginLostYourPassword());
        highLight(accountPage.getRegisterEmailTextBox());
        highLight(accountPage.getRegisterUsernameTextBox());
        highLight(accountPage.getRegisterPasswordTextBox());
        highLight(accountPage.getRegisterButton());
        highLight(accountPage.getPrivacyPolicyLink());

        Thread.sleep(10000);

    }

    @Test(priority = 3)
    public void Products() throws InterruptedException {

        Store store = new Store(getDriver());
        Navigator navigator = new Navigator(getDriver());

        navigator.goToStorePage();
        System.out.println("Numbers Of Products " + store.getProductsImg().size() );

        for (int i = 0; i < store.getProductsImg().size();i++){
            highLight(store.getProductsImg().get(i));
            highLightBackground(store.getProductsTitleText().get(i));
        }
        Thread.sleep(10000);

}

    @Test(priority = 4)
    public void productsHighLights() throws InterruptedException  {
        Store store = new Store(getDriver());
        Product product = new Product(getDriver());
        Navigator  navigator = new Navigator(getDriver());

        navigator.goToStorePage();
        store.getProductsImg().get(3).click();

        highLight(product.getAddToCart());
        highLight(product.getQuantity());
        mouseHoverOver(product.getQuantity());

        Thread.sleep(10000);// just to make sure elements located correctly
    }

    @Test(priority = 5)
    public void cartHighLights() throws InterruptedException {

        AccountPage accountPage = new AccountPage(getDriver());
        Product product = new Product(getDriver());
        Navigator navigator = new Navigator(getDriver());
        Cart cart = new Cart(getDriver());
        Store store = new Store(getDriver());

        String email = "Faisal_787@Arafeh.com";
         String password = accountPage.getUserPassword();

         accountPage.logInAUser(email,password);
         navigator.goToStorePage();
         store.getProductsImg().get(3).click();
         product.getAddToCart().click();
         navigator.goToCartPage();
         highLight(cart.getCancelItemIcon());

        Thread.sleep(10000);

    }

    @Test
    public void starsReviewHighLight() throws InterruptedException {

        Product product = new Product(getDriver());
        Navigator  navigator = new Navigator(getDriver());
        Store   store = new Store(getDriver());

        navigator.goToStorePage();
        store.getProductsImg().get(5).click();
        product.getReviewsTap().click();



        for (int i = 0; i < product.getReviewRatingStars().size();i++) {
            highLight(product.getReviewRatingStars().get(i));
        }
        Thread.sleep(10000);
    }

    public void highLight(WebElement webElement) {

        JavascriptExecutor Js = (JavascriptExecutor) getDriver();


        Js.executeScript("arguments[0].style.border = '3px solid yellow'", webElement);
    }

    public void highLightBackground(WebElement webElement) {

        JavascriptExecutor Js = (JavascriptExecutor) getDriver();


        Js.executeScript("arguments[0].setAttribute('style', 'background: orange; border: 2px solid orange;');", webElement);
    }

    public Actions mouseHoverOver(WebElement element) {


        Actions action = new Actions(getDriver());

        action.moveToElement(element).build().perform();
        return action;
    }

}
