package Test;

import Utilities.BrowsersBase;
import Utilities.DataProviderUtility;
import Utilities.Navigator;
import WebElementsUtilities.Product;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ReviewsTest extends BrowsersBase {

    Product product = null;
    Navigator navigator = null;

    @Test(priority = 16, dataProviderClass = DataProviderUtility.class, dataProvider = "testData", alwaysRun = true)
    public void productReview(Object starsRating ,Object reviewText, Object name,Object email) {

        product = new Product(getDriver());
        product.review(starsRating,reviewText,name,email);

        Assert.assertTrue(product.getReviewAwaitingApproval().isDisplayed());
    }

    @Test(priority = 17, alwaysRun = true)
    public void ReviewWithRating() {

        product = new Product(getDriver());
        navigator = new Navigator(getDriver());
        product.review("Great Product","Faisal","Arafeh@Gmail.com");

        Assert.assertEquals(navigator.getAlertText(), "Please select a rating");
        navigator.alertAccept();
    }

}
