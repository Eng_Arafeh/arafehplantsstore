package Test;

import Utilities.BrowsersBase;
import WebElementsUtilities.AccountPage;
import Utilities.Navigator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BrowsersBase {

    Navigator navigator;
    AccountPage accountPage = new AccountPage(getDriver());
    public String getEmail() {return email;}
    private String email = "Faisal_787@Arafeh.com";
    private String password = accountPage.getUserPassword();
    private String inValidPassword = "#$d1dsa536GBSJ##@";
    private String inValidEmail = "Arafeh@QA.com";
    private String expectedResult = "Faisal_";


    @Test(priority = 8, alwaysRun = true)
    public void validLogin() {
        accountPage = new AccountPage(getDriver());
        accountPage.logInAUser(email, password);

        Assert.assertTrue(accountPage.getActualResult().contains(expectedResult)); }

    @Test(priority = 9, alwaysRun = true)
    public void inValidPasswordLogin() {
        accountPage = new AccountPage(getDriver());
        accountPage.logInAUser(email, inValidPassword);
        String expectedResult = "Error: The password you entered for the email address "
                + email + " is incorrect. Lost your password?";

        Assert.assertEquals(accountPage.getErrorMessage(), expectedResult); }

    @Test(priority = 10, alwaysRun = true)
    public void emptyCredentialsLogin() {
        accountPage = new AccountPage(getDriver());
        accountPage.logInAUser("", "");

        Assert.assertEquals(accountPage.getErrorMessage(), "Error: Username is required.");}

    @Test(priority = 11, alwaysRun = true)
    public void emptyPasswordLogin() {
        accountPage = new AccountPage(getDriver());
        accountPage.logInAUser(email, "");
        String errorExpectedResult = "Error: The password field is empty.";
        String actualResult = accountPage.getErrorMessage();

        Assert.assertEquals(actualResult, errorExpectedResult);}

    @Test(priority = 12, alwaysRun = true)
    public void invalidLogin() {
        accountPage = new AccountPage(getDriver());
        accountPage.logInAUser(inValidEmail, inValidPassword);
        String errorExpectedResult = "Unknown email address. Check again or try your username.";
        String actualResult = accountPage.getErrorMessage();

        Assert.assertEquals(actualResult, errorExpectedResult);}

    @Test(priority = 13, alwaysRun = true)
    public void passwordAsterisk() {
        accountPage = new AccountPage(getDriver());
        navigator = new Navigator(getDriver());
        navigator.goToMyAccountPage();
        accountPage.getLoginPasswordTextBox().sendKeys(password);
        boolean isEncrypted = accountPage.getLoginPasswordTextBox().getAttribute("type").equals("password");

        Assert.assertTrue(isEncrypted);}

    @Test(priority = 14, alwaysRun = true)
    public void goBackAfterLogin() {
        accountPage = new AccountPage(getDriver());
        navigator = new Navigator(getDriver());
        accountPage.logInAUser(email, password);
        navigator.browserBackButton();
        navigator.goToMyAccountPage();

        Assert.assertTrue(accountPage.getActualResult().contains(expectedResult));}

    @Test(priority = 15, alwaysRun = true)
    public void newTapLogin() {
        accountPage = new AccountPage(getDriver());
        navigator = new Navigator(getDriver());
        navigator.openWebElementInNewTap(navigator.getMyAccountPage());
        navigator.goToBrowserTap(1);
        accountPage.logInAUser(email, password);

        Assert.assertTrue(accountPage.getActualResult().contains(expectedResult));}

}