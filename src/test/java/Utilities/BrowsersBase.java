package Utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.*;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class BrowsersBase {

	private final  String URL = "http://arafeh.apptrainers.com/";
	protected static ThreadLocal<WebDriver> driver = new ThreadLocal<>();
	public void setDriver(WebDriver driver){this.driver.set(driver);}
	public WebDriver getDriver(){return this.driver.get();}
	private static String ProjectPath = System.getProperty("user.dir");
	private final String BrowserProfilePath = ProjectPath.replaceAll("arafehplants","BrowserProfile");
	//private static String browser = "Chrome";

	@BeforeMethod
	@Parameters("browser") //String browser
	public void lunchBrowser(String browser) {

		ChromeOptions chromeOptions = new ChromeOptions();
		FirefoxOptions firefoxOptions = new FirefoxOptions();

		if (browser.equalsIgnoreCase("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			chromeOptions.addArguments("--start-maximized");
			setDriver(new FirefoxDriver(firefoxOptions));}

		else if (browser.equalsIgnoreCase("Chrome")) {
			WebDriverManager.chromedriver().setup();
			chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			chromeOptions.setExperimentalOption("useAutomationExtension", false);
			chromeOptions.addArguments("--start-maximized");
			setDriver(new ChromeDriver(chromeOptions));}

		else if (browser.equalsIgnoreCase("ChromeWithProfile")) {
			WebDriverManager.chromedriver().setup();
			chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			chromeOptions.setExperimentalOption("useAutomationExtension", false);
			chromeOptions.addArguments("--start-maximized");
			chromeOptions.addArguments("--user-data-dir="+ BrowserProfilePath+"/chromeProfile");
			setDriver(new ChromeDriver(chromeOptions));}

		else if (browser.equalsIgnoreCase("Edg")) {
			WebDriverManager.edgedriver().setup();

			setDriver(new EdgeDriver());
			getDriver().manage().window().maximize();
		}

		else if (browser.equalsIgnoreCase("ChromeDriver")) {
			System.setProperty("webdriver.chrome.driver", ProjectPath + "/src/main/resources/Drivers/chrome/chromedriver89.exe");
			setDriver(new ChromeDriver());}

		else if (browser.equalsIgnoreCase("ChromeDriverWithProfile")) {
			System.setProperty("webdriver.chrome.driver",
					ProjectPath + "/src/main/resources/Drivers/chrome/chromedriver89.exe");
			chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			chromeOptions.setExperimentalOption("useAutomationExtension", false);
			chromeOptions.addArguments("--start-maximized");
			chromeOptions.addArguments("user-data-dir="+ProjectPath+"/src/main/resources/BrowserProfiles/chromeProfile");
			setDriver(new ChromeDriver(chromeOptions));}

		else if (browser.equalsIgnoreCase("IDEDriver")) {
			System.setProperty("webdriver.ie.driver",
					ProjectPath + "/src/main/resources/Drivers/IDE/IEDriverServer.exe");
			setDriver(new InternetExplorerDriver());}

		else if (browser.equalsIgnoreCase("FireFoxDriver")) {
			System.setProperty("webdriver.gecko.driver",
					ProjectPath + "/src/main/resources/Drivers/GeckoFireFox/geckodriver.exe");
			setDriver(new FirefoxDriver());}

		else if (browser.equalsIgnoreCase("Chrome-headless")) {
			WebDriverManager.chromedriver().setup();
			chromeOptions.addArguments("--headless");
			chromeOptions.addArguments("window-size=1366,768");
			setDriver(new ChromeDriver(chromeOptions));}

		goToURL();
		implicitWait();
	}
	@AfterMethod
	public void tearDown() {getDriver().quit();}

	private void goToURL(){
		getDriver().get(URL);}

	private void implicitWait(){
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public String getURL() {
		return URL;
	}
}