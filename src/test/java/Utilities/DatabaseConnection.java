package Utilities;

import java.sql.*;

public class DatabaseConnection {

    private final String URL = "http://ns3746.hostgator.com/";
    private final String DATABASE = "smart83_wrdp8";
    private final String USERNAME = "smart83_wrdp8";
    private final String PASSWORD = "jvQki3BvLYOIjmCe";
    private String RESULTS = null;

    public String JDBC_SELECT(String selectQuery, String columnName) throws SQLException {

        Connection connection = DriverManager.getConnection("jdbc:mysql://" + URL + "/" + DATABASE, USERNAME, PASSWORD);
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery(selectQuery);

        while (resultSet.next()) {
            if (resultSet.isLast()) {
                RESULTS = resultSet.getString(columnName);
            }
        }
        connection.close();
        statement.close();

        return RESULTS;
    }
}
