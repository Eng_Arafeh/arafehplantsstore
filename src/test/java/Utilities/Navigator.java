package Utilities;

import org.openqa.selenium.*;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class Navigator extends BrowsersBase {
    public Navigator(WebDriver driver) {
        PageFactory.initElements(driver, this);}

    @CacheLookup
    @FindBy(xpath = "//img[@class='custom-logo']")
    private WebElement logoIcon;
    public void clickOnLogoIcon() {
        logoIcon.click();
    }
    public WebElement getLogoIcon() {
        return logoIcon;
    }

    @CacheLookup
    @FindBy(xpath = "//a[@class='menu-link']")
    private WebElement homePage;
    public void goToHomePage() { homePage.click(); }
    public WebElement getHomePage() {
        return homePage;
    }

    @CacheLookup
    @FindBy(xpath = "(//a[@class='menu-link'])[2]")
    private WebElement storePage;
    public void goToStorePage() { storePage.click(); }
    public WebElement getStorePage() {
        return storePage;
    }

    @CacheLookup
    @FindBy(xpath = "(//a[@class='menu-link'])[3]")
    private WebElement menPage;
    public void goToMenPage() { menPage.click(); }
    public WebElement getMenPage() {
        return menPage;
    }

    @CacheLookup
    @FindBy(xpath = "//li[@id='menu-item-267']/a[1]")
    private WebElement womenPage;
    public void goToWomenPagePage() { womenPage.click(); }
    public WebElement getWomenPagePage() {
        return womenPage;
    }

    @CacheLookup
    @FindBy(xpath = "//li[@id='menu-item-671']/a[1]")
    private WebElement accessoriesPage;
    public void goToAccessoriesPage() { accessoriesPage.click(); }
    public WebElement getAccessoriesPage() {
        return accessoriesPage;
    }

    @CacheLookup
    @FindBy(xpath = "//li[@id='menu-item-825']/a[1]")
    private WebElement myAccountPage;
    public void goToMyAccountPage() { myAccountPage.click(); }
    public WebElement getMyAccountPage() {
        return myAccountPage;
    }

    @CacheLookup
    @FindBy(xpath = "//li[@id='menu-item-828']/a[1]")
    private WebElement aboutPage;
    public void goToAboutPage() { aboutPage.click(); }
    public WebElement getAboutPage() {
        return aboutPage;
    }

    @CacheLookup
    @FindBy(xpath = "//li[@id='menu-item-829']/a[1]")
    private WebElement contactUsPage;
    public void goToContactUsPage() { contactUsPage.click(); }
    public WebElement getContactUsPage() {
        return contactUsPage;
    }

    @CacheLookup
    @FindBy(xpath = "//header/div[@id='ast-desktop-header']/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/a[1]/div[1]/span[1]")
    private WebElement cartPage;
    public void goToCartPage() { cartPage.click(); }
    public WebElement getCartPage() {
        return cartPage;
    }

    public void openWebElementInNewTap(WebElement webElement) {
        webElement.sendKeys(Keys.chord(Keys.CONTROL, Keys.RETURN));}

    public void goToBrowserTap(int tapIndex) {
        ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(tapIndex));}

    public void browserBackButton(){
        for (int i = 0; i <= 1; i++) {
            getDriver().navigate().back();}}

    public void alertAccept() {
        getDriver().switchTo().alert().accept();
    }

    public String getAlertText() {
        return  getDriver().switchTo().alert().getText();
    }

    public void openTap() {
        ((JavascriptExecutor) getDriver()).executeScript("window.open()");
    }

    public void closeTap() {getDriver().close();}

    public void expectedWait(By element, int time) {
       WebDriverWait webDriverWait = new WebDriverWait(getDriver(), time);
       webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(element));


    }
}