package Utilities;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelUtility {
	
	private FileInputStream FIS;
	private FileOutputStream FOS;
	private XSSFWorkbook workBook;
	private XSSFSheet sheet;
	private XSSFRow row;
	private XSSFCell cell;
	
	String excelPath = null;
	
	  public ExcelUtility(String excelPath) {
		this.excelPath = excelPath;
	}
	  
	  public int getRowCount(String sheetName) throws IOException
	  {
		  
		  FIS = new FileInputStream(excelPath);
		  workBook = new XSSFWorkbook(FIS);
		  sheet = workBook.getSheet(sheetName);
		  int rowCount = sheet.getLastRowNum();
		  workBook.close();
		  FIS.close();
		  return rowCount;
		  
	  }
	  
	  public int getCellCount(String sheetName, int rowNum) throws IOException
	  {
		  
		  FIS = new FileInputStream(excelPath);
		  workBook = new XSSFWorkbook(FIS);
		  sheet = workBook.getSheet(sheetName);
		  row= sheet.getRow(rowNum);
		  int cellCount = row.getLastCellNum();
		  workBook.close();
		  FIS.close();
		  return cellCount;
		  
	  }

	  public Object getCellData(String sheetName, int rowNum, int colNum) throws IOException // String or use Object
	  {
		  FIS = new FileInputStream(excelPath);
		  workBook = new XSSFWorkbook(FIS);
		  sheet = workBook.getSheet(sheetName);
		  row= sheet.getRow(rowNum);
		  cell = row.getCell(colNum);
		  
		  DataFormatter dataFormatter = new DataFormatter(); // convert data to object or string
		  Object data = null; // can change the data type to int, boolean, String or double ...etc
		  
		  try {
			 data = dataFormatter.formatCellValue(cell);
			  }
		  catch (Exception e){
			  data = "No Data Was Found";
		  }
		  workBook.close();
		  FIS.close();
		  return data;
	  }
	
	  public void setCellData(String sheetName, int rowNum, int colNum, String data) throws IOException
	  {
		  FIS = new FileInputStream(excelPath);
		  workBook = new XSSFWorkbook(FIS);
		  sheet = workBook.getSheet(sheetName);
		  
		  row= sheet.getRow(rowNum);
		  cell = row.createCell(colNum);
		  cell.setCellValue(data);
		  FOS = new FileOutputStream(excelPath);
		  workBook.write(FOS);

		  workBook.close();
		  FIS.close();
		  FOS.close();
	  }


}
