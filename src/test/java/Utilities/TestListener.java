package Utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import java.io.IOException;

public class TestListener extends  BrowsersBase implements ITestListener{

	screenShots screenShots = new screenShots();

public void onTestStart(ITestResult result) {

	System.setProperty("org.uncommons.reportng.title", "Arafeh TestNG Report");
		
	Reporter.log("The Method Name Is: "+ result.getName());
	
	System.out.println("onTestStart");
	
	}
	
public void onTestSuccess(ITestResult result) {
	
	Reporter.log("The Test Status Is: "+ result.getStatus());

	System.out.println("onTestSuccess");

	}

public void onTestFailure(ITestResult result)  {

	
	System.out.println("Test Failure Happened");

	String ProjectPath = System.getProperty("user.dir"); // find project path

	System.setProperty("org.uncommons.reportng.escape-output", "false"); 
	Reporter.log("<a href=\""+ProjectPath+"\\screenShot\\\">All Fails Screen Shots File.     ||  </a>");

	Reporter.log("<a href= file:///"+ProjectPath+"/test-output/index.html#\">  ||  Stander Report     </a>");

}

public void onTestSkipped(ITestResult result) {
	
	System.out.println("onTestSkipped");
	getDriver().quit();

	}

public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	System.out.println("onTestFailedButWithinSuccessPercentage");

	}

public void onTestFailedWithTimeout(ITestResult result) {
	System.out.println("onTestFailedWithTimeout");

	}

public void onStart(ITestContext context) {
	System.out.println("onStart");

	}

public void onFinish(ITestContext context) {

	System.out.println("onFinish");

	}
	public void onIgnored(ITestContext context) {
		System.out.println("onIgnored");

	}

}
