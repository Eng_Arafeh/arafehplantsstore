package Utilities;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class screenShots extends BrowsersBase  {

	private  DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
	private  Date date = new Date();
	private String screenShotFileName = dateFormat.format(date).toString().replace(" ",
			"_").replace(":", "_").replace("-", "_");
	public void getScreenShot() throws IOException {
		File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
			FileUtils.copyFile(screenShot, new File("./ScreenShot/"+screenShotFileName+".png"));
	}
	public String getScreenShotFileName() {
		return screenShotFileName;
	}
}
