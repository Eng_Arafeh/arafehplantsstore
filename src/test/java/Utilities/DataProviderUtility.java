package Utilities;

import org.testng.annotations.DataProvider;
import java.io.IOException;

public class DataProviderUtility {

    @DataProvider(name="testData")
    public Object[][] data() throws IOException {
       String path = "./src/main/resources/ExcelFiles/Review.xlsx";
        String sheetName = "Sheet1";

        ExcelUtility excelUtility = new ExcelUtility(path);
        int totalRows = excelUtility.getRowCount(sheetName);
        int totalCols = excelUtility.getCellCount(sheetName,1);
        Object Data[][] = new String [totalRows][totalCols];
        for(int i =1; i<=totalRows;i++) {
            for(int j=0;j<totalCols;j++) {
                Data[i-1][j] = excelUtility.getCellData(sheetName, i, j);
            }
        }
        return Data;
    }
}
