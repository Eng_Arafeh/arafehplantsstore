package WebElementsUtilities;

import Utilities.BrowsersBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Checkout extends BrowsersBase {


    public Checkout(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @CacheLookup
    @FindBy(className = "showcoupon")
    private WebElement clickHereToEnterYourCode_LinkText;


    @CacheLookup
    @FindBy(id = "billing_first_name")
    private WebElement firstNameTextBox;

    @CacheLookup
    @FindBy(id = "billing_last_name")
    private WebElement lastNameTextBox;

    @CacheLookup
    @FindBy(id = "billing_company")
    private WebElement companyNameTextBox;

    @CacheLookup
    @FindBy(id = "select2-billing_country-container")
    private WebElement countriesList;

    @CacheLookup
    @FindBy(xpath = "//input[@role='combobox']")
    private WebElement countriesSearchTextBox;

    public WebElement getCountriesList() {
        return  countriesList;
    }
    public WebElement getCountriesSearchTextBox() {
        return  countriesSearchTextBox;
    }

    @CacheLookup
    @FindBy(id = "billing_address_1")
    private WebElement streetAddressTextBox;

    @CacheLookup
    @FindBy(id = "billing_address_2")
    private WebElement apartmentAddressTextBox;


    @CacheLookup
    @FindBy(id = "billing_city")
    private WebElement cityTextBox;


    @CacheLookup
    @FindBy(id = "billing_state")
    private WebElement stateTextBox;

    @CacheLookup
    @FindBy(id = "billing_postcode")
    private WebElement postcodeTextBox;

    @CacheLookup
    @FindBy(id = "billing_phone")
    private WebElement phoneTextBox;

    @CacheLookup
    @FindBy(id = "billing_email")
    private WebElement emailTextBox;

    @CacheLookup
    @FindBy(id = "ship-to-different-address-checkbox")
    private WebElement shipToDifferentAddress_Checkbox;

    @CacheLookup
    @FindBy(id = "order_comments")
    private WebElement orderNotesTexBox;

    @CacheLookup
    @FindBy(name = "woocommerce_checkout_place_order")
    private WebElement placeOrderButton;

    @CacheLookup
    @FindBy(xpath = "//p[contains(text(),'Thank you. Your order has been received.')]")
    private WebElement successCheckOutMessage;

    public WebElement getClickHereToEnterYourCode_LinkText() {
        return clickHereToEnterYourCode_LinkText;
    }

    public WebElement getFirstNameTextBox() {
        firstNameTextBox.clear();
        return firstNameTextBox;
    }

    public WebElement getLastNameTextBox() {
        lastNameTextBox.clear();
        return lastNameTextBox;
    }

    public WebElement getCompanyNameTextBox() {
        companyNameTextBox.clear();
        return companyNameTextBox;
    }


    public WebElement getStreetAddressTextBox() {
        streetAddressTextBox.clear();
        return streetAddressTextBox;
    }

    public WebElement getApartmentAddressTextBox() {
        apartmentAddressTextBox.clear();
        return apartmentAddressTextBox;
    }

    public WebElement getCityTextBox() {
        cityTextBox.clear();
        return cityTextBox;
    }

    public WebElement getStateTextBox() {
        stateTextBox.clear();
        return stateTextBox;
    }

    public WebElement getPostcodeTextBox() {
        postcodeTextBox.clear();
        return postcodeTextBox;
    }

    public WebElement getPhoneTextBox() {
        phoneTextBox.clear();
        return phoneTextBox;
    }

    public WebElement getEmailTextBox() {
        emailTextBox.clear();
        return emailTextBox;
    }

    public WebElement getShipToDifferentAddress_Checkbox() {
        return shipToDifferentAddress_Checkbox;
    }

    public WebElement getOrderNotesTexBox() {
        orderNotesTexBox.clear();
        return orderNotesTexBox;
    }

    public WebElement getPlaceOrderButton() {
        return placeOrderButton;
    }

    public String getSuccessCheckOutMessage() {
        return successCheckOutMessage.getText();
    }

}