package WebElementsUtilities;

import Utilities.BrowsersBase;
import Utilities.Navigator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountPage extends BrowsersBase {

    Navigator navigator;

    public AccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @CacheLookup
    @FindBy(id = "username")
    private WebElement loginEmailAddressTextBox;
    public String getUserName() {
        return userName;
    }

    @CacheLookup
    @FindBy(id = "password")
    private WebElement loginPasswordTextBox;

    @CacheLookup
    @FindBy(xpath = "//input[@id='rememberme']")
    private WebElement rememberMeCheckBox;

    @CacheLookup
    @FindBy(name = "login")
    private WebElement loginButton;

    @CacheLookup
    @FindBy(linkText = "Lost your password?")
    private WebElement loginLostYourPassword;

    @CacheLookup
    @FindBy(id = "reg_username")
    private WebElement registerUsernameTextBox;

    @CacheLookup
    @FindBy(id = "reg_email")
    private WebElement registerEmailTextBox;

    @CacheLookup
    @FindBy(id = "reg_password")
    private WebElement registerPasswordTextBox;

    @FindBy(name = "register")
    private WebElement registerButton;

    @CacheLookup
    @FindBy(className = "woocommerce-privacy-policy-link")
    private WebElement privacyPolicyLink;

    @CacheLookup
    @FindBy(linkText = "Log out")
    private WebElement logout;
    public WebElement getLogout() {
        return logout;
    }

    @CacheLookup
    @FindBy(linkText = "Lost your password?")
    private WebElement lostYourPassword;

    public WebElement getLostYourPassword() {
        return lostYourPassword;
    }

    @FindBy(tagName = "strong")
    private WebElement UsernameWasUsedToLoginOrRegister;
    public String getActualResult() {
        return UsernameWasUsedToLoginOrRegister.getText();
    }

    @FindBy(xpath = "//div[@id='content']//li[1]")
    private WebElement errorMessage;
    public String getErrorMessage() {
        return errorMessage.getText();
    }

    @FindBy(tagName = "small")
    private WebElement passwordHint;

    // Generate A Random Number
    private final int lower = 1000;
    private final int higher = 100000;
    private int randomNumber = (int) (Math.random() * (higher * lower)) - lower;

    private String userEmail = "Faisal_" + randomNumber + "@ArafehShopping.Com";
    private String userName = "Faisal_" + randomNumber;
    private final String userPassword = "Pa$$W0rd_6454$#*^s";

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getPasswordHint() {
        return passwordHint.getText();
    }

    public void registerANewUser(String userName, String email, String password) {
        navigator = new Navigator(getDriver());
        navigator.goToMyAccountPage();
        registerUsernameTextBox.sendKeys(userName);
        registerEmailTextBox.sendKeys(email);
        registerPasswordTextBox.sendKeys(password);
        registerButton.click();

    }

    public void logInAUser(String userNameOrEmail, String password) {

        Navigator navigator = new Navigator(getDriver());

        navigator.goToMyAccountPage();

        loginEmailAddressTextBox.sendKeys(userNameOrEmail);
        loginPasswordTextBox.sendKeys(password);
        loginButton.click();
    }


    public void loginAUserWithRememberMeCheckBox(String userNameOrEmail, String password) {

        navigator = new Navigator(getDriver());
        navigator.goToMyAccountPage();
        loginEmailAddressTextBox.sendKeys(userNameOrEmail);
        loginPasswordTextBox.sendKeys(password);
        rememberMeCheckBox.click();
        loginButton.click();
        getDriver().quit();
        getDriver().get(getURL());
    }


    public WebElement getLoginEmailAddressTextBox() {
        return loginEmailAddressTextBox;
    }

    public WebElement getLoginPasswordTextBox() {
        return loginPasswordTextBox;
    }

    public WebElement getRememberMeCheckBox() {
        return rememberMeCheckBox;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public WebElement getLoginLostYourPassword() {
        return loginLostYourPassword;
    }

    public WebElement getRegisterUsernameTextBox() {
        return registerUsernameTextBox;
    }

    public WebElement getRegisterEmailTextBox() {
        return registerEmailTextBox;
    }

    public WebElement getRegisterPasswordTextBox() {
        return registerPasswordTextBox;
    }

    public WebElement getRegisterButton() {
        return registerButton;
    }

    public WebElement getPrivacyPolicyLink() {
        return privacyPolicyLink;
    }

    public WebElement getUsernameWasUsedToLoginOrRegister() {
        return UsernameWasUsedToLoginOrRegister;
    }

    public int getRandomNumber() {
        return randomNumber;
    }

}





