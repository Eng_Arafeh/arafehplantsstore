package WebElementsUtilities;

import Utilities.BrowsersBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;
import java.util.List;

public class Store extends BrowsersBase{

    public Store(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @CacheLookup
    @FindBy(xpath = "//body/div[@id='page']/div[@id='content']/div[1]/div[2]/main[1]/div[1]/ul[1]/li")
    private List<WebElement> allProductsImg;
    public List<WebElement> getProductsImg() {return allProductsImg;}

     @CacheLookup
     @FindBy(tagName = "h2")
     private List<WebElement> allProductsTitleText;
     public List<WebElement> getProductsTitleText() {return allProductsTitleText;}

    @CacheLookup
    @FindBy(xpath = "//input[@id='quantity_609ffca6ee4cd']")
    private WebElement quantity;

    public WebElement getQuantity() {
        return quantity;
    }



}
