package WebElementsUtilities;

import Utilities.BrowsersBase;
import Utilities.Navigator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;

public class ForgetPassword extends BrowsersBase {

    public ForgetPassword() {
        PageFactory.initElements(getDriver(), this);
    }

    @CacheLookup
    @FindBy(id = "user_login")
    private WebElement emailToChangePasswordTextBox;

    @CacheLookup
    @FindBy(tagName = "button")
    private WebElement resetPasswordButton;

    @CacheLookup
    @FindBy(xpath = "(//a[@class='internal sign-in-link'])[2]")
    private WebElement outLookSignIn;

    @CacheLookup
    @FindBy(name = "loginfmt")
    private WebElement outLookEmailTextBox;

    @CacheLookup
    @FindBy(id = "idSIButton9")
    private WebElement outLookNextButton;

    @CacheLookup
    @FindBy(name = "passwd")
    private WebElement outLookPasswordTextBox;

    @CacheLookup
    @FindBy(id = "idSIButton9")
    private WebElement outLookSignInButton;

    Navigator navigator = new Navigator(getDriver());
    AccountPage accountPage = new AccountPage(getDriver());

    public void loginAfterChangingPassword(String changePasswordEmail, String newPassword){
        navigator = new Navigator(getDriver());
        navigator.goToMyAccountPage();
        accountPage.getLostYourPassword().click();
        emailToChangePasswordTextBox.sendKeys(changePasswordEmail);
        resetPasswordButton.click();

        openNewEmptyTap();

        ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));

        String accountUrl = "http://outlook.com/";
        getDriver().navigate().to(accountUrl);

//        accountPage.getLoginEmailAddressTextBox().sendKeys(changePasswordEmail);
//        accountPage.getLoginPasswordTextBox().sendKeys(newPassword);
//        accountPage.getLoginButton().click();
    }
    public void openNewEmptyTap() {
        ((JavascriptExecutor) driver).executeScript("window.open()");
    }
}
