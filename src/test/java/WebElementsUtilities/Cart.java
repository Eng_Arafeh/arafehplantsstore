package WebElementsUtilities;

import Utilities.BrowsersBase;
import Utilities.Navigator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class Cart extends BrowsersBase{

    Navigator navigator = null;
    Store store = null;
    AccountPage accountPage = null;

    public Cart(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @CacheLookup
    @FindBy(name = "apply_coupon")
    private WebElement applyCouponButton;

    @CacheLookup
    @FindBy(name = "update_cart")
    private WebElement updateCartButton;

    @CacheLookup
    @FindBy(linkText = "Proceed to checkout")
    private WebElement proceedToCheckoutButton;

    @CacheLookup
    @FindBy(linkText = "×")
    private WebElement cancelItemIcon;

    @CacheLookup
    @FindBy(xpath = "//input[@id='quantity_60a144d778d09']")
    private List<WebElement> quantity;

    @CacheLookup
    @FindBy(className = "woocommerce-message")
    private WebElement cartMessage;
    public String getCartMessage() {
        return cartMessage.getText();
    }


    public WebElement getCancelItemIcon() {
        return cancelItemIcon;
    }

    public WebElement getProceedToCheckoutButton() {
        return proceedToCheckoutButton;
    }

}
