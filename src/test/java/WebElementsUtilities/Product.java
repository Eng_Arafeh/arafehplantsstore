package WebElementsUtilities;

import Utilities.BrowsersBase;
import Utilities.Navigator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class Product extends BrowsersBase {

    Navigator navigator;
    Store store = null;
    AccountPage accountPage = null;
    Cart cart = null;
    Checkout checkout = null;

    public Product(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @CacheLookup
    @FindBy(xpath = "//input[contains(@class,'input-text qty')]")
    private WebElement quantity;

    @CacheLookup
    @FindBy(name = "add-to-cart")
    private WebElement addToCartButton;

    @CacheLookup
    @FindBy(xpath = "(//a[@class='button wc-forward'])[2]")
    private WebElement viewCartButton;

    @CacheLookup
    @FindBy(xpath = "//a[@href='#tab-reviews']")
    private WebElement reviewsTap;

    @CacheLookup
    @FindBy(xpath = "//a[@href='#tab-description']")
    private WebElement descriptionTap;

    @CacheLookup
    @FindAll({@FindBy(className = "star-1"), @FindBy(className = "star-2"), @FindBy(className = "star-3"),
            @FindBy(className = "star-4"), @FindBy(className = "star-5")})
    private List<WebElement> reviewRatingStars;

    @CacheLookup
    @FindBy(id = "comment")
    private WebElement reviewTextBox;

    @CacheLookup
    @FindBy(id = "author")
    private WebElement nameTextBox;

    @CacheLookup
    @FindBy(id = "email")
    private WebElement emailTextBox;

    @CacheLookup
    @FindBy(id = "wp-comment-cookies-consent")
    private WebElement emailCheckBox;

    @CacheLookup
    @FindBy(xpath = "//p[@class='meta']//em")
    private WebElement reviewAwaitingApproval;

    @CacheLookup
    @FindBy(id = "submit")
    private WebElement submitButton;

    public WebElement getReviewsTap() {
        return reviewsTap;
    }

    public WebElement getAddToCart() {
        return addToCartButton;
    }

    public WebElement getDescriptionTap() {
        return descriptionTap;
    }

    public WebElement getQuantity() {
        return quantity;
    }

    public List<WebElement> getReviewRatingStars() {
        return reviewRatingStars;
    }

    public WebElement getReviewTextBox() {
        reviewTextBox.click();
        return reviewTextBox;
    }

    public WebElement getNameTextBox() {
        nameTextBox.click();
        return nameTextBox;
    }

    public WebElement getEmailTextBox() {
        emailTextBox.click();
        return emailTextBox;
    }

    public WebElement getReviewAwaitingApproval() {

        return reviewAwaitingApproval;
    }

    public void product(String email, String password, int productIndex, String country) {

        Actions action = new Actions(getDriver());
        store = new Store(getDriver());
        navigator = new Navigator(getDriver());
        accountPage = new AccountPage(getDriver());
        cart = new Cart(getDriver());
        checkout = new Checkout(getDriver());
        navigator.goToMyAccountPage();
        accountPage.logInAUser(email, password);
        navigator.goToStorePage();
        store.getProductsImg().get(7).click();
        quantity.clear();
        quantity.sendKeys("3");
        addToCartButton.click();
        navigator.goToCartPage();
        cart.getProceedToCheckoutButton().click();
        checkout.getFirstNameTextBox().sendKeys("Faisal");
        checkout.getLastNameTextBox().sendKeys("Arafeh");
        checkout.getCountriesList().click();
        checkout.getCountriesSearchTextBox().sendKeys(country);
        action.sendKeys(Keys.ENTER).perform();
        checkout.getStreetAddressTextBox().sendKeys("Amman Street");
        checkout.getCityTextBox().sendKeys("Amman");
        checkout.getStateTextBox().sendKeys("Amman");
        checkout.getPostcodeTextBox().sendKeys("4532");
        checkout.getPhoneTextBox().sendKeys("07972356585");
        navigator.expectedWait(By.className("blockUI blockOverlay"),4);
        checkout.getPlaceOrderButton().click();
    }

    public void product(String email, String password) {

        store = new Store(getDriver());
        navigator = new Navigator(getDriver());
        accountPage = new AccountPage(getDriver());
        cart = new Cart(getDriver());
        checkout = new Checkout(getDriver());
        navigator.goToMyAccountPage();
        accountPage.logInAUser(email, password);
        navigator.goToStorePage();
        store.getProductsImg().get(7).click();
        quantity.clear();
        quantity.sendKeys("3");
        addToCartButton.click();
        navigator.goToCartPage();
        cart.getCancelItemIcon().click();

    }

    public void review(Object starsRating, Object reviewText_, Object name_, Object email_) {

        navigator = new Navigator(getDriver());
        accountPage = new AccountPage(getDriver());
        store = new Store(getDriver());
        store = new Store(getDriver());
        navigator.goToStorePage();

        String reviewText = reviewText_.toString();
        String name = name_.toString();
        String email = email_.toString();
        String randomNumber = Integer.toString(accountPage.getRandomNumber());

        int starsRatingInt = Integer.parseInt(String.valueOf(starsRating));


        store.getProductsImg().get(5).click();
        getReviewsTap().click();
        getReviewRatingStars().get(starsRatingInt).click();
        getReviewTextBox().sendKeys(reviewText + " " + randomNumber);
        getNameTextBox().sendKeys(name);
        getEmailTextBox().sendKeys(email);
        submitButton.click();

    }

    public void review(String reviewText, String name, String email) {

        navigator = new Navigator(getDriver());
        accountPage = new AccountPage(getDriver());
        store = new Store(getDriver());
        store = new Store(getDriver());
        navigator.goToStorePage();
        String randomNumber = Integer.toString(accountPage.getRandomNumber());
        store.getProductsImg().get(5).click();
        getReviewsTap().click();
        getReviewTextBox().sendKeys(reviewText + " " + randomNumber);
        getNameTextBox().sendKeys(name);
        getEmailTextBox().sendKeys(email);
        submitButton.click();

    }

}
